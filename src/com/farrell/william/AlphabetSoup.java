/**
 * 
 */
package com.farrell.william;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author William Farrell
 * Coding Challenge for Enlighten
 *
 */
public class AlphabetSoup {
	
	private static PuzzleGrid puzzleGrid;
	private static List<SearchableWord> searchableWordList;

	/**
	 * @param accepts path/file name of word search puzzle config file
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		if(args == null || args.length != 1) {
			String errMsg = "Must provide path and file name for configuration file";
			System.out.println(errMsg);
			throw new Exception(errMsg);
		}
		String configFile = args[0];

		loadPuzzleData(configFile);
		findMatches();
		for (SearchableWord searchableWord: searchableWordList) {
			if (searchableWord.isWordBackwards()) {
				System.out.println (searchableWord.getWord() + " " + searchableWord.getEndRow() + ":" + searchableWord.getEndCol()
				+ " " + searchableWord.getStartRow() + ":" + searchableWord.getStartCol());
			}
			else {
				System.out.println (searchableWord.getWord() + " " + searchableWord.getStartRow() + ":" + searchableWord.getStartCol()
				+ " " + searchableWord.getEndRow() + ":" + searchableWord.getEndCol());
			}
			
		}


	}

	private static void findMatches() {
		for (SearchableWord searchableWord: searchableWordList) {
			findHorizontalMatch(searchableWord);
			if (! searchableWord.isWordFound()) {
				findVerticalMatch(searchableWord);
			}
			if (! searchableWord.isWordFound()) {
				findDiagonalMatch(searchableWord);
			}
		}
		
	}

	private static void findDiagonalMatch(SearchableWord searchableWord) {
		findDiagonalMatchLR(searchableWord);
		if (! searchableWord.isWordFound()) {
			findDiagonalMatchRL(searchableWord);
		} 
		
	}

	private static void findDiagonalMatchRL(SearchableWord searchableWord) {
		// Look for diagonal match starting in top left corner to lower right
				int r = 0;
				char[][] gridData = puzzleGrid.getGridData();
				while ( r < (puzzleGrid.getTotalRows() - 1) && ! searchableWord.isWordFound()) {
					
					int startRow = r;
					//iterate grid going right to left across columns within row r
					for (int startCol = puzzleGrid.getTotalColumns() - 1; startCol > searchableWord.getWord().length()-1; startCol--) {
						// find diagonal value left to right starting at startRow, startCol
						int holdRow = startRow;
						String diagValue = "";
						for (int holdCol = startCol;holdCol > 0 ; holdCol--) {
							diagValue = diagValue + gridData[holdRow][holdCol];
							if (holdRow < puzzleGrid.getTotalRows() - 1) {
								holdRow++;
							}
						}
						int forwardResult = diagValue.indexOf(searchableWord.getWord());
						if (forwardResult >=0) {
							int endRow = startRow + forwardResult + searchableWord.getWord().length() - 1;
							int endCol = startCol - forwardResult + searchableWord.getWord().length() - 1;
							searchableWord.setEndRow(endRow);
							searchableWord.setEndCol(endCol);
							searchableWord.setStartRow(startRow + forwardResult);
							searchableWord.setStartCol(startCol - forwardResult);
							searchableWord.setWordFound(true);
						}else {
							int reverseResult = diagValue.indexOf(searchableWord.getReverseWord());
							if (reverseResult >=0) {
								int endRow = startRow +reverseResult + searchableWord.getWord().length() - 1;
								int endCol = startCol - reverseResult + searchableWord.getWord().length() - 1;
								searchableWord.setEndRow(endRow);
								searchableWord.setEndCol(endCol);
								searchableWord.setStartRow(startRow + reverseResult);
								searchableWord.setStartCol(startCol + reverseResult);
								// word found backwards - swap coordinates when results displayed
								searchableWord.setWordBackwards(true);
								searchableWord.setWordFound(true);
							}
						};
					};
				// look at next row
					r++;
					
				}

		
	}

	private static void findDiagonalMatchLR(SearchableWord searchableWord) {
		// Look for diagonal match starting in top left corner to lower right
		int r = 0;
		char[][] gridData = puzzleGrid.getGridData();
		while ( r < (puzzleGrid.getTotalRows() - 1) && ! searchableWord.isWordFound()) {
			
			int startRow = r;
			//iterate grid going left to right across columns within row r
			for (int startCol = 0; startCol <= puzzleGrid.getTotalColumns() - searchableWord.getWord().length(); startCol++) {
				// find diagonal value left to right starting at startRow, startCol
				int holdRow = startRow;
				String diagValue = "";
				for (int holdCol = startCol;holdCol <= puzzleGrid.getTotalColumns() - 1 ; holdCol++) {
					diagValue = diagValue + gridData[holdRow][holdCol];
					if (holdRow < puzzleGrid.getTotalRows() - 1) {
						holdRow++;
					}
				}
				int forwardResult = diagValue.indexOf(searchableWord.getWord());
				if (forwardResult >=0) {
					int endRow = startRow + forwardResult + searchableWord.getWord().length() - 1;
					int endCol = startCol+ forwardResult + searchableWord.getWord().length() - 1;
					searchableWord.setEndRow(endRow);
					searchableWord.setEndCol(endCol);
					searchableWord.setStartRow(startRow + forwardResult);
					searchableWord.setStartCol(startCol + forwardResult);
					searchableWord.setWordFound(true);
				}else {
					int reverseResult = diagValue.indexOf(searchableWord.getReverseWord());
					if (reverseResult >=0) {
						int endRow = startRow +reverseResult + searchableWord.getWord().length() - 1;
						int endCol = startCol+ reverseResult + searchableWord.getWord().length() - 1;
						searchableWord.setEndRow(endRow);
						searchableWord.setEndCol(endCol);
						searchableWord.setStartRow(startRow + reverseResult);
						searchableWord.setStartCol(startCol + reverseResult);
						// word found backwards - swap coordinates when results displayed
						searchableWord.setWordBackwards(true);
						searchableWord.setWordFound(true);
					}
				};
			};
		// look at next row
			r++;
			
		}

		
	}

	private static void findVerticalMatch(SearchableWord searchableWord) {
		int c = 0;
		char[][] gridData = puzzleGrid.getGridData();
		while ( c < (puzzleGrid.getTotalColumns() - 1) && ! searchableWord.isWordFound()) {
			// get row Value
			String colVal = "";			
			for (int r=0; r< puzzleGrid.getTotalColumns(); r++) {
				   colVal = colVal + gridData[r][c];
				}
			int forwardResult = colVal.indexOf(searchableWord.getWord()); 
			if (forwardResult >=0) {
				int endRow = forwardResult + searchableWord.getWord().length() - 1;
				searchableWord.setEndRow(endRow);
				searchableWord.setEndCol(c);
				searchableWord.setStartRow(forwardResult);
				searchableWord.setStartCol(c);
				searchableWord.setWordFound(true);
			}
			else {
				int reverseResult = colVal.indexOf(searchableWord.getReverseWord());
				if (reverseResult >=0) {
					int endRow = reverseResult + searchableWord.getWord().length() - 1;
					searchableWord.setEndRow(endRow);
					searchableWord.setEndCol(c);
					searchableWord.setStartRow(reverseResult);
					searchableWord.setStartCol(c);
					// word found backwards - swap coordinates when results displayed
					searchableWord.setWordBackwards(true);
					searchableWord.setWordFound(true);
				}
			}
			c++;
		}
		
	}

	private static void findHorizontalMatch(SearchableWord searchableWord) {
		int r = 0;
		char[][] gridData = puzzleGrid.getGridData();
		while ( r < (puzzleGrid.getTotalRows() ) && ! searchableWord.isWordFound()) {
			// get row Value
			String rowVal = "";			
			for (int c=0; c< puzzleGrid.getTotalColumns(); c++) {
				   rowVal = rowVal + gridData[r][c];
				}
			int forwardResult = rowVal.indexOf(searchableWord.getWord()); 
			if (forwardResult >=0) {
				int endCol = forwardResult + searchableWord.getWord().length() - 1;
				searchableWord.setEndRow(r);
				searchableWord.setEndCol(endCol);
				searchableWord.setStartRow(r);
				searchableWord.setStartCol(forwardResult);
				searchableWord.setWordFound(true);
			}
			else {
				int reverseResult = rowVal.indexOf(searchableWord.getReverseWord());
				if (reverseResult >=0) {
					int endCol = reverseResult + searchableWord.getWord().length() - 1;
					searchableWord.setEndRow(r);
					searchableWord.setEndCol(endCol);
					searchableWord.setStartRow(r);
					searchableWord.setStartCol(reverseResult);
					// word found backwards - swap coordinates when results displayed
					searchableWord.setWordBackwards(true);
					searchableWord.setWordFound(true);
				}
			}
			r++;
		}
		
		
		
	}

	private static void loadPuzzleData(String fileName) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			String[] dimensionArray = in.readLine().split("x");
			int rowCount = Integer.parseInt(dimensionArray[0]);
			int colCount = Integer.parseInt(dimensionArray[1]);
			//create puzzleGrid object
			puzzleGrid = new PuzzleGrid(rowCount, colCount);
			char[][] gridData = new char[rowCount][colCount];
			for (int row = 0; row < rowCount; row++) {
				//split returns string, result should be array of 1 letter strings
				String [] letters = in.readLine().split(" ");
				int col = 0;
				for (String letter: letters) {
					gridData[row][col] = Character.toUpperCase(letter.charAt(0));
					col++;
				}
			}
			puzzleGrid.setGridData(gridData);
			String inString = null;
			//create and populate searchableWordList
			searchableWordList = new ArrayList<SearchableWord>();
			while((inString=in.readLine())!=null) {
				String word = inString.replaceAll(" ", "");
				String reverseWord =  new StringBuilder(word).reverse().toString();
				SearchableWord searchableWord = new SearchableWord(word, reverseWord);
				searchableWordList.add(searchableWord);
			}
			in.close();		
		} catch (IOException e) {
			System.out.println(e);
		}
		
	}


}
