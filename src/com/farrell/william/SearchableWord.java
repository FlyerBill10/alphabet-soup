package com.farrell.william;

public class SearchableWord {
	
	private String word;
	//value of word reversed
	private String reverseWord;
	// start row/column of word
	private Integer startRow;
	private Integer startCol;
	// ending row/column of word
	private Integer endRow;
	private Integer endCol;
	// has word been found so we can quit looking?
	private boolean isWordFound;
	//is word found backwards? (we'll need to reverse end/starting coordinates for display)
	private boolean isWordBackwards;
	public SearchableWord(String word, String reverseWord) {
		super();
		this.word = word;
		this.reverseWord = reverseWord;
		this.isWordFound = false;
		this.isWordBackwards = false;		
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getReverseWord() {
		return reverseWord;
	}
	public void setReverseWord(String reverseWord) {
		this.reverseWord = reverseWord;
	}
	public Integer getStartRow() {
		return startRow;
	}
	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}
	public Integer getStartCol() {
		return startCol;
	}
	public void setStartCol(Integer startCol) {
		this.startCol = startCol;
	}
	public Integer getEndRow() {
		return endRow;
	}
	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}
	public Integer getEndCol() {
		return endCol;
	}
	public void setEndCol(Integer endCol) {
		this.endCol = endCol;
	}
	public boolean isWordFound() {
		return isWordFound;
	}
	public void setWordFound(boolean isWordFound) {
		this.isWordFound = isWordFound;
	}
	public boolean isWordBackwards() {
		return isWordBackwards;
	}
	public void setWordBackwards(boolean isWordBackwards) {
		this.isWordBackwards = isWordBackwards;
	}
	
	
	
	

}
