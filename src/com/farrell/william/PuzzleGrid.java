/**
 * 
 */
package com.farrell.william;

/**
 * @author William Farrell
 * 
 * Data about size and contents of puzzleGrid
 */
public class PuzzleGrid {
	private Integer totalRows;
	private Integer totalColumns;
	private char[][] gridData;
	public PuzzleGrid(Integer totalRows, Integer totalColumns) {
		super();
		this.totalRows = totalRows;
		this.totalColumns = totalColumns;
	}
	public Integer getTotalRows() {
		return totalRows;
	}
	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}
	public Integer getTotalColumns() {
		return totalColumns;
	}
	public void setTotalColumns(Integer totalColumns) {
		this.totalColumns = totalColumns;
	}
	public char[][] getGridData() {
		return gridData;
	}
	public void setGridData(char[][] gridData) {
		this.gridData = gridData;
	}
	
	
	
}
